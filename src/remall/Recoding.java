/* 
 * Copyright (C) 2018 Miika Tapio < miika.tapio at luke.fi > 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package remall;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;

/**
 *
 * @author Miika Tapio < miika.tapio at luke.fi >
 */
public class Recoding {

    public Options options;

    public Recoding(Options options) {
        this.options = options;
    }

    public void doRecoding(Options options) {

        try {
            options.setMessage("");
            int ignoredcols = Integer.valueOf(options.getIgnoredcolsString());
            if (ignoredcols < 1) {
                options.setMessage(ErrorMessages.IN_ERRORHEAD3);
            }
            if (options.getMessage().isEmpty()) {
                ErrorMessages.checkInDataFile(options);
                //System.out.println("Important1: " + options.getMessage());
                if (options.getMessage().isEmpty()) {
                    ErrorMessages.checkMapFile(options);
                    // System.out.println("Important2: " + options.getMessage());
                    if (options.getMessage().isEmpty()) {
                        // System.out.println("Important3: " + options.getMessage() +" !");

                        /* 
            Read data in 
                         */
                        //int ignoredcols = Integer.valueOf(options.getIgnoredcolsString());
                        DataClass oldformat = new DataClass(options.getIndatafile(), ignoredcols);
                        TreeSet<String> markerSet = new TreeSet<>();
                        ArrayList<String> headers = new ArrayList<>(oldformat.getFilledHeaderList());
                        for (int i = ignoredcols; i < headers.size(); i++) {
                            markerSet.add(headers.get(i));
                        }
                        options.setMessage(oldformat.getMessage());
                        /* 
            Read recoding map in 
                         */
                        AllMap map = new AllMap(options.getInmapfile());
                        /* 
            Recode
                         */
                        DataClass newformat = new DataClass(options.getOutdatafile(), oldformat, map, ignoredcols);
                        /* 
            Write output data 
                         */
                        System.out.println("");
                        Write.datamatrix(options.getOutdatafile(), newformat);

                        /* 
            Collect marker information for the input data (Does not influence the changing later.
                         */
                        ArrayList<Marker> markerList = new ArrayList<>();
                        Iterator<String> iterator = markerSet.iterator();

                        Integer current = 0;
                        while (iterator.hasNext()) {
                            markerList.add(new Marker(iterator.next(), oldformat, ignoredcols));
                            current++;
                        }
                        /* 
            Collect marker information for the output data 
                         */

                        ArrayList<ChangedMarker> markerListNew = new ArrayList<>();
                        Iterator<String> iteratorNew = markerSet.iterator();

                        current = 0;
                        while (iteratorNew.hasNext()) {
                            markerListNew.add(new ChangedMarker(iteratorNew.next(), newformat, ignoredcols, newformat.getUnmappedAlleles()));
                            current++;
                        }


                        /* 
            Write change summary 
                         */
                        PrintStream console = System.out;
                        File file = new File(options.getOutsummaryfile());
                        FileOutputStream fos = new FileOutputStream(file);
                        PrintStream ps = new PrintStream(fos);
                        System.setOut(ps);
                        System.out.println("\nSummary\n\nInput data ");
                        RaportMethods.printBasicFileMetadata(options.getIndatafile());
                        System.out.println("Input map");
                        RaportMethods.printBasicFileMetadata(options.getInmapfile());
                        System.out.println("Output data");
                        RaportMethods.printBasicFileMetadata(options.getOutdatafile());

                        System.out.println("Below is shown for each marker the old allele (frequency) and new allele (frequency),\nseparated by\n"
                                + "==> if allele code was changed following a given recoding rule\n"
                                + "<=> if allele code was unchanged following a given recoding rule and\n"
                                + "<-> if allele code was not changed because there was no recoding rule\n");

                        System.out.println("Markers:" + markerSet.toString());

                        for (current = 0; current < markerList.size(); current++) {
                            RaportMethods.printChangeSummaryPerMarker(markerList.get(current), markerListNew.get(current), map);
                        }
                        System.setOut(console);
                    }
                }
            }
        } catch (IOException | NumberFormatException ex) {
            System.out.println(ex.getMessage());
        }
    }

}

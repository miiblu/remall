/* 
 * Copyright (C) 2018 Miika Tapio < miika.tapio at luke.fi > 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package remall;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;

/**
 *
 * @author Miika Tapio < miika.tapio at luke.fi >
 */
public class ErrorMessages {

    public static final String IN_ERROR = "Input file is not in the correct format!";
    public static final String INMAP_ERROR = "Recoding allele map file beginning is not in the correct format!";
    public static final String INMAP_ERROR2 = "Recoding allele map file is not in the correct format!";
    public static final String INMAP_ERRORALL = "Recoding map file is not in the correct format! Double instruction.";
    public static final String COUNT_ERROR = "Problem in counting alleles!";
    public static final String IN_ERRORHEAD = "Note, changing did not begin from the first column of a marker!\nCheck recoded data and recoding summary.\nThis can be a header problem or selected number of ignored columns problem.";
    public static final String IN_ERRORHEAD2 = "Input problem. Check headers and selected number of ignored columns!";
    public static final String IN_ERRORHEAD3 = "Selected number of ignored columns must be >0!";


    /**
     * Get the value of inputErrorMessage
     *
     * @return the value of inputErrorMessage
     */
    static void checkInDataFile(Options options) throws FileNotFoundException, IOException {
        int ignoredcols = Integer.valueOf(options.getIgnoredcolsString());

        BufferedReader bReader;
        bReader = new BufferedReader(new FileReader(options.getIndatafile()));
        String line;
        line = bReader.readLine();
        String[] splitted = line.split("\\t", -1);
        if (splitted.length <= ignoredcols) {
            options.setMessage(ErrorMessages.IN_ERRORHEAD2);
            System.out.println(ErrorMessages.IN_ERRORHEAD2);
            return;
        }
        if (!splitted[0].equals("id")) {
            options.setMessage(ErrorMessages.IN_ERROR);
            System.out.println(ErrorMessages.IN_ERROR);
            return;
        }
    }

    static void checkMapFile(Options options) throws FileNotFoundException, IOException {

        BufferedReader bReader;
        bReader = new BufferedReader(new FileReader(options.getInmapfile()));
        String line;
        line = bReader.readLine();
        String[] splitted = line.split("\\t", -1);
        if (!splitted[0].equals("Marker")) { //check if Marker is the first header to confirm file type

            options.setMessage(ErrorMessages.INMAP_ERROR);
            System.out.println(ErrorMessages.INMAP_ERROR);
            //return;
        }
        if (splitted.length != 3) { // check if the first line has 3 columns to confirm file type
            options.setMessage(ErrorMessages.INMAP_ERROR);
            System.out.println(ErrorMessages.INMAP_ERROR);
            //return;
        }
        HashSet<String> setTwo = new HashSet<>();
        while ((line = bReader.readLine()) != null) {
            splitted = line.split("\\t", -1);
            //System.out.println(splitted[0] + "&" + splitted[1]);
            String two = splitted[0] + "_" + splitted[1];
            if (splitted.length != 3) {
                options.setMessage(ErrorMessages.INMAP_ERROR2);
                System.out.println(ErrorMessages.INMAP_ERROR2); // Does not have three columns everywhere
               // return;
            }
            //System.out.println(two); 
            //HashSet<String> setTwo = new HashSet<>();
            Boolean wasEmpty = setTwo.add(two);
            if (!wasEmpty) {
                options.setMessage(ErrorMessages.INMAP_ERRORALL);
                System.out.println(ErrorMessages.INMAP_ERRORALL); // Conflicting instructions
               // return;
            }

        }   //while ends
    }

}

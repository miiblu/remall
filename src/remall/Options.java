/* 
 * Copyright (C) 2018 Miika Tapio < miika.tapio at luke.fi > 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package remall;

/**
 *
 * @author Miika Tapio < miika.tapio at luke.fi >
 */
public class Options {
    private String indatafile;
    private String inmapfile;
    private String outdatafile;
    private String ignoredcolsString;
    private String outsummaryfile;
    private String message;

    public Options() {
        this.indatafile = "";
        this.inmapfile = "";
        this.outdatafile = "";
        this.ignoredcolsString = "";
    }

    public void setIndatafile(String indatafile) {
        this.indatafile = indatafile;
    }

    public void setOutsummaryfile(String outsummaryfile) {
        this.outsummaryfile = outsummaryfile;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setInmapfile(String inmapfile) {
        this.inmapfile = inmapfile;
    }

    public void setOutdatafile(String outdatafile) {
        this.outdatafile = outdatafile;
    }

    public void setIgnoredcolsString(String ignoredcolsString) {
        this.ignoredcolsString = ignoredcolsString;
    }

    public String getIndatafile() {
        return indatafile;
    }

    public String getInmapfile() {
        return inmapfile;
    }

    public String getOutsummaryfile() {
        return outsummaryfile;
    }

    public String getOutdatafile() {
        return outdatafile;
    }

    public String getIgnoredcolsString() {
        return ignoredcolsString;
    }
    
}

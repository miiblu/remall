/* 
 * Copyright (C) 2018 Miika Tapio < miika.tapio at luke.fi > 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package remall;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;

/**
 * Marker information for the changed marker. Extends Marker.
 * @author Miika Tapio < miika.tapio at luke.fi >
 */
public class ChangedMarker extends Marker {

    private final ArrayList<MarkerAllele> unmapped;
/**
 * Marker information for the changed marker. Extends Marker.
 * @param markername
 * @param data
     * @param ignoredCols
 * @param unmapped 
 */
    public ChangedMarker(String markername, DataClass data, Integer ignoredCols, ArrayList<MarkerAllele> unmapped) {
        super(markername, data,ignoredCols);
        this.unmapped = new ArrayList<>();/**(new Comparator<MarkerAllele>() {
        @Override
        public int compare(MarkerAllele marall1, MarkerAllele marall2) {
            byte[] marall1b = new byte[marall1.getMarkerBytes().length + marall1.getAlleleBytes().length];
            for (int i = 0; i < marall1b.length; ++i) {
                marall1b[i] = i < marall1.getMarkerBytes().length ? marall1.getMarkerBytes()[i] : marall1.getAlleleBytes()[i - marall1.getMarkerBytes().length];
            }

            byte[] marall2b = new byte[marall2.getMarkerBytes().length + marall2.getAlleleBytes().length];
            for (int i = 0; i < marall2b.length; ++i) {
                marall2b[i] = i < marall2.getMarkerBytes().length ? marall2.getMarkerBytes()[i] : marall2.getAlleleBytes()[i - marall2.getMarkerBytes().length];
            }

            String first = new String(marall1b,StandardCharsets.UTF_8);
            String second = new String(marall2b,StandardCharsets.UTF_8);
            if (first.equals(second)) {
                return 0;
            } else {
                int com = first.compareTo(second);
                if (com < 0) {
                    return 1;
                } else {
                    return -1;
                }
            }
        }
    }
    );
        unmapped.forEach((marall) -> {
            addToSetOfUnmapped(marall);
        });
**/
    }

    public void addToSetOfUnmapped(MarkerAllele marall) {
        unmapped.add(marall);
    }

    @Override
    public String toString() {
        return "Marker{" + "markername=" + this.getMarkername() + ", colIndList=" + this.getColIndList() + ", alleleSet=" + this.getAlleleSet()
                + ", alleleFreqList=" + this.getAlleleFreqList() + ", Unmapped alleles=" + this.unmapped + '}';
    }

}

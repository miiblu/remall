/* 
 * Copyright (C) 2018 Miika Tapio < miika.tapio at luke.fi > 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package remall;

import java.nio.charset.StandardCharsets;

/**
 * This is the key for the hashMap that changes allele coding and to store
 * information of alleles not in the map file.
 *
 * @author Miika Tapio < miika.tapio at luke.fi >
 */
public class MarkerAllele {

    private byte[] marker;
    private byte[] allele;

    public void setMarker(String marker) {
        this.marker = marker.getBytes(StandardCharsets.UTF_8);
    }

    public void setAllele(String allele) {
        this.allele = allele.getBytes(StandardCharsets.UTF_8);
    }

    public String getMarker() {
        return new String(marker,StandardCharsets.UTF_8);
    }

    public String getAllele() {
        return new String(allele,StandardCharsets.UTF_8);
    }
    
    public byte[] getMarkerBytes() {
        return marker;
    }

    public byte[] getAlleleBytes() {
        return allele;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MarkerAllele) {
            MarkerAllele s = (MarkerAllele) obj;
            return getMarker().equals(s.getMarker()) && getAllele().equals(s.getAllele());
        }
        return false;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        return getMarker().hashCode() * 31 + getAllele().hashCode();
    }

    @Override
    public String toString() {
        return getMarker() + getAllele();
    }

}

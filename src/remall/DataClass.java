/* 
 * Copyright (C) 2018 Miika Tapio < miika.tapio at luke.fi > 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package remall;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Miika Tapio < miika.tapio at luke.fi >
 */
public final class DataClass {

    private String filepathname;
    private ArrayList<Column> dataList = new ArrayList<>();
    private ArrayList<String> headerList = new ArrayList<>();
    private ArrayList<String> filledHeaderList = new ArrayList<>();
    private ArrayList<MarkerAllele> unmappedAlleles = new ArrayList<>();
    private String message="";

    /**
     *
     * @param filepathname
     * @param ignoredCols
     * @throws FileNotFoundException
     * @throws IOException
     */
    public DataClass(String filepathname, Integer ignoredCols) throws FileNotFoundException, IOException {
        this.filepathname = filepathname;
        this.unmappedAlleles = null;

        try ( /**
                 * Creating a buffered reader to read the file
                 */
                BufferedReader bReader = new BufferedReader(new FileReader(filepathname))) {
            String line;

            /**
             * Looping the read block until all lines in the file are read.
             */
            line = bReader.readLine();
            String[] splitted = line.split("\\t", -1);
            for (int i = 0; i < splitted.length; i++) {
                // check if dataList begins ok
                if (i == 0) {
                    if (!splitted[i].equals("id")) {
                        throw new IllegalArgumentException(ErrorMessages.IN_ERROR); // the first header is not "id", suggests wrong file
                    }
                } // Input begins ok generate all columns
                Column column = new Column(splitted[i]);
                // Add column into list of columns
                dataList.add(column);
                // System.out.println(dataList.toString());

            }
            while ((line = bReader.readLine()) != null) {
                splitted = line.split("\\t",-1);
                for (int x = 0; x < splitted.length; x++) {
                    dataList.get(x).addData(splitted[x]);
                }
            }   //while ends
        }
  //      rmEmptyFColumns(dataList); // remove empty columns from the end, to tolerate some spreadsheet "miss-behavior"
        makeHeaderList();
        makeFilledHeaderList(getHeaderList(), ignoredCols);
        if (ignoredCols>headerList.size()) {
           setMessage(ErrorMessages.IN_ERRORHEAD2);
           throw new IllegalArgumentException(ErrorMessages.IN_ERRORHEAD2); // all columns ignored
        }

    }

    /**
     * Constructor with new file name and previously generated object of
     * DataClass type and AllMap type object makes a recoded DataClass type
     * object, calling private method recodeDataList
     *
     * @param filepathname
     * @param oldData
     * @param map
     * @param ignoredCols
     */
    public DataClass(String filepathname, DataClass oldData, AllMap map, Integer ignoredCols) {
        this.filepathname = filepathname;
        this.headerList = oldData.getHeaderList();
        this.filledHeaderList = oldData.getFilledHeaderList();
        this.dataList = recodeDataList(oldData, map, ignoredCols);


    }

    String getFilepathname() {
        return filepathname;
    }

    /**
     * Set the value of filepathname
     *
     * @param filepathname new value of filepathname
     */
    public void setFilepathname(String filepathname) {
        this.filepathname = filepathname;
    }

    public ArrayList<Column> getDataList() {
        return dataList;
    }

    private void makeHeaderList() {
        for (int i = 0; i < dataList.size(); i++) {
            headerList.add(dataList.get(i).getHeader());
        }
    }

    public ArrayList<String> getHeaderList() {
        return headerList;
    }
    
        public ArrayList<String> getFilledHeaderList() {
        return filledHeaderList;
    }

    public int size() {
        return dataList.size();
    }

    private ArrayList<Column> recodeDataList(DataClass oldData, AllMap map, Integer ignoredCols) { 
        ArrayList<String> headers = oldData.getFilledHeaderList();
        ArrayList<Column> newDataList = new ArrayList<>();

        for (int i = 0; i < headers.size(); i++) {  //i is column
            Column column = new Column(headers.get(i));

            newDataList.add(column);
        }

        for (int i = 0; i < headers.size(); i++) {

            for (int x = 0; x < oldData.getDataList().get(1).getDataRowNo(); x++) {
                if (i < ignoredCols) { 
                    newDataList.get(i).addData(oldData.getDataList().get(i).getData(x));
                } else {
                    newDataList.get(i).addData(map.getNewAllele(headers.get(i), oldData.getDataList().get(i).getData(x)));
                   if (newDataList.get(i).getData(x).isEmpty()) { 
                        newDataList.get(i).replaceLastData(oldData.getDataList().get(i).getData(x));
                        MarkerAllele notInMap = new MarkerAllele();
                        notInMap.setMarker(headers.get(i));
                        notInMap.setAllele(oldData.getDataList().get(i).getData(x));
                        unmappedAlleles.add(notInMap);
                    }
                }
            }
        }

        return newDataList;
    }

    @Override
    public String toString() {
        return "DataClass:\n" + "filepathname=" + filepathname + "\nContains dataList: \n" + this.getDataList();
    }

    public ArrayList<MarkerAllele> getUnmappedAlleles() {
        return unmappedAlleles;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    private void makeFilledHeaderList(ArrayList<String> headerList, Integer ignoredCols) {
        Iterator<String> iterator = headerList.iterator();
        if (headerList.get(ignoredCols).isEmpty()) {
           setMessage(ErrorMessages.IN_ERRORHEAD);
        }
        int ind = 0;
        while (iterator.hasNext()) {
         //   if (ind < ignoredCols) {
         //       filledHeaderList.add(iterator.next());
         //       ind++;
         //   } else {
                String head= iterator.next();
                if (!head.isEmpty()) {
                    filledHeaderList.add(head);
                    ind++;
                } else {
                    filledHeaderList.add(filledHeaderList.get(ind-1));
                    ind++;
                }
         //   }
        }
  }

    private void rmEmptyFColumns(ArrayList<Column> dataList) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

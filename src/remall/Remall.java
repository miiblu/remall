/* 
 * Copyright (C) 2018 Miika Tapio < miika.tapio at luke.fi > 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package remall;

import java.awt.GraphicsEnvironment;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Miika Tapio < miika.tapio at luke.fi >
 */
public class Remall {

    /**
     * @param args the command line arguments
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        List<String> argsList = new ArrayList<>();
        HashMap<String, String> optsMap = new HashMap<>();
        if (args.length == 0) {
            if (GraphicsEnvironment.isHeadless()) {
                System.out.println("Graphics are not available for GUI. Expected 4 arguments,input data file (-d), map file (-m), output file (-o) and skipped columns number(-s), for example 'java -jar remall.jar -d input.csv -m map.csv -o output.csv -s 1'");
                System.exit(1);
            } else {
                 Options goptions = new Options();
                 Gui_remall form;
                form = new Gui_remall(goptions);
                goptions.setMessage("");
                form.setVisible(true);

            }
        } else {

            if (args.length != 8) {
                System.out.println("Expected 4 arguments,input data file (-d), map file (-m), output file (-o) and skipped columns number(-s), for example 'java -jar remall.jar -d input.csv -m map.csv -o output.csv -s 1'");
                System.exit(1);
            }

            for (int i = 0; i < args.length; i++) {
                switch (args[i].charAt(0)) {
                    case '-':
                        if (args[i].length() < 2) {
                            throw new IllegalArgumentException("Not a valid argument: " + args[i]);
                        }

                        if (args.length - 1 == i) {
                            throw new IllegalArgumentException("Expected arg after: " + args[i]);
                        }
                        // -opt
                        optsMap.put(args[i], args[i + 1]);
                        i++;

                        break;

                    default:
                        throw new IllegalArgumentException("Expected arguments for input data file (-d), map file (-m), output file (-o) and skipped columns number(-s), for example 'java -jar remall.jar -d input.csv -m map.csv -o output.csv -s 1'");

                }
            }

            //"./examplefiles/data.csv"; 
            // "./examplefiles/map.csv";
            //"./examplefiles/out.csv";
            Options coptions = new Options();
            coptions.setMessage("");
           // String indatafile = optsMap.get("-d"); //"./examplefiles/data.csv";
            coptions.setIndatafile(optsMap.get("-d"));
            if (optsMap.get("-d") == null) {
                System.out.println("Expected an input data argument (-d).");
                System.exit(1);
            }
            String inmapfile = optsMap.get("-m");//"./examplefiles/map.csv";
            coptions.setInmapfile(optsMap.get("-m"));
            if (optsMap.get("-m") == null) {
                System.out.println("Expected an map data argument (-m).");
                System.exit(1);
            }
            String outdatafile = optsMap.get("-o");
            coptions.setOutdatafile(optsMap.get("-o"));
            if (optsMap.get("-o") == null) {
                System.out.println("Expected an output data argument (-o).");
                System.exit(1);
            }
            //String ignoredcolsString = optsMap.get("-s");
            coptions.setIgnoredcolsString(optsMap.get("-s"));
            if (optsMap.get("-s")== null) {
                System.out.println("Expected an skip number of columsn argument (-s).");
                System.exit(1);
            }

            String outsummaryfile = "";
            int p = outdatafile.lastIndexOf(".");
            String end = outdatafile.substring(p + 1);
            if (p == -1 || !end.matches("\\w+")) {
                //outsummaryfile = outdatafile + "_summary.txt";
                coptions.setOutsummaryfile(outdatafile + "_recoding_summary.txt");
            } else {
                //outsummaryfile = outdatafile.substring(0, p) + "_summary.txt";
                coptions.setOutsummaryfile(outdatafile.substring(0, p) + "_recoding_summary.txt");

            }
            //String outsummaryfile = outdatafile.split(Pattern.quote("."))[1] + "_summary.txt";

            try {
                int ignoredcols = Integer.valueOf(coptions.getIgnoredcolsString());
                //int ignoredcols = Integer.valueOf(coptions.getIgnoredcolsString());
            } catch (NumberFormatException nfe) {
                System.out.println("The -s argument must be an integer.");
                System.exit(1);
            }

            int ignoredcols = Integer.valueOf(coptions.getIgnoredcolsString());


            Recoding recoding=new Recoding(coptions);
            recoding.doRecoding(coptions);
                        if (!coptions.getMessage().isEmpty()) {
                                                    throw new IllegalArgumentException(coptions.getMessage()); 
                        }
            System.out.println("Done");

        }
    }
}

/* 
 * Copyright (C) 2018 Miika Tapio < miika.tapio at luke.fi > 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package remall;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;

/**
 * Contains and collects per marked information based on an DataClass object
 *
 * @author Miika Tapio < miika.tapio at luke.fi >
 */
public class Marker {
    private final String markername;
    private ArrayList<Boolean> colIndList;
    private TreeSet<String> alleleSet;
    private ArrayList<Integer> alleleFreqList;


    public Marker(String markername, DataClass data, Integer ignoredCols) {
        this.markername = markername;
        this.colIndList = new ArrayList<>();
        this.colIndList = makeColIndList(data, ignoredCols);
        this.alleleSet = new TreeSet<>();
        this.alleleSet = makeAlleleSet(data);
        this.alleleFreqList = new ArrayList<>();
        this.alleleFreqList = makeAlleleFreqList(data);
        if (this.alleleSet.first().equals("")) {alleleFreqList.set(0, calculateNulls(data, this.colIndList));}
    }

    /**
     * Add an allele for the marker.
     *
     * @param allele
     */
    private void addAllele(String allele) {
        alleleSet.add(allele);
    }
/**
 * Get Indices where the marker data is within the DataClass type object
 * @return colIndList made by makeColIndList method
 */
    public ArrayList<Boolean> getColIndList() {
        return colIndList;
    }

    /**
     * Make the ArrayList with the indices for the marker data within the DataClass type object
     * @param data 
     * @return Constructed ArrayList of indices
     */
    private final ArrayList<Boolean> makeColIndList(DataClass data, int ignoredCols) {
        ArrayList<Boolean> inds = new ArrayList<>();
        for (int i = 0; i < data.getDataList().size(); i++) {
            if (i<ignoredCols) {
                inds.add(false);
            } else {
            inds.add(data.getFilledHeaderList().get(i).equals(markername));
            }
        }
        return inds;
    }
/**
 * Constructs the unique list of alleles in the DataClass data.
 * @param data
 * @return allelesSet which can be accessed by the getAlleleSet -method
 */
    private TreeSet<String> makeAlleleSet(DataClass data) {
        Iterator<Boolean> iterator = colIndList.iterator();
        TreeSet<String> allelesSet = new TreeSet<>();
        boolean include = false;
        int col = 0;
        while (iterator.hasNext()) {
            include = iterator.next();
            if (include) {
                for (int j = 0; j < data.getDataList().get(col).getDataRowNo(); j++) {
                    allelesSet.add(data.getDataList().get(col).getData(j));
                }
            }
            col++;
        }
        return allelesSet;
    }
/**
 * Get the marker name of the object
 * @return markername
 */
    public String getMarkername() {
        return markername;
    }
/**
 * Get the AlleleSet of the marker.
 * @return alleleSet
 */
    public TreeSet<String> getAlleleSet() {
        return alleleSet;
    }
/**
 * Get the allele frequencies for the marker.
 * @return alleleFreqList
 */
    public ArrayList<Integer> getAlleleFreqList() {
        return alleleFreqList;
    }


    private ArrayList<Integer> makeAlleleFreqList(DataClass data) {
        Iterator<Boolean> colIterator = colIndList.iterator();
        ArrayList<Integer> freqList = new ArrayList<>();
        int col = 0;
        while (colIterator.hasNext()) { 
             boolean include = colIterator.next();
            if (include) {
                Iterator<String> allIterator = alleleSet.iterator();
                                //System.out.println("col: "+col);
                int all = 0;
                while (allIterator.hasNext()) {
                                     //int count = getRepeatCountColumn(data.getDataList().get(col).getDataColumnAsString(), allIterator.next());
                                     int count = getRepeatCountColumn(data.getDataList().get(col), allIterator.next());
                    //System.out.println("all: "+ all + "  count: "+count);
                    if (freqList.size() < all + 1 || freqList.isEmpty()) {
                        freqList.add(count);
                    } else {
                        freqList.set(all, freqList.get(all) + count);
                    }
                    all++;
                }
            }

            col++;
        }
        //System.out.println("freqList: " + freqList);
        return freqList;

    }

        /**
     * This will count number of times an allele will occur in a column
     *
     * @param columntext is the column as text format separated by ";"
     * @param findAllele is counted allele code as string
     * @return
     */
        public int getRepeatCountColumnOld(String columntext, String findAllele) {
        int count = 0;
        if (!columntext.isEmpty() && findAllele != null && !findAllele.isEmpty() && columntext.contains(findAllele)) {
            count = (columntext.split(";"+findAllele+";",-1)).length - 1; //
        }
        return count;
    }
        public int getRepeatCountColumn(Column column, String findAllele) {
        int count = 0;
        for (byte[] temp: column.getDataList()){
            //System.out.println(new String(temp,StandardCharsets.UTF_8));
          if (findAllele.equals(new String(temp,StandardCharsets.UTF_8) )) count++;  
        }
        
        //{
        //    count = (columntext.split(";"+findAllele+";",-1)).length - 1; //
        //}
        return count;
    }

    
    @Override
    public String toString() {
        return "Marker{" + "markername=" + markername + ", colIndList=" + colIndList + ", alleleSet=" + alleleSet + ", alleleFreqList=" + alleleFreqList + '}';
    }

     private int calculateTotal(ArrayList<Integer> alleleFreqList) {
        int sum = 0;
        sum = alleleFreqList.stream().map((d) -> d).reduce(sum, Integer::sum);
        return sum;
    }
     
    private int calculateNulls(DataClass data, ArrayList<Boolean> colIndList) {  //tässä ei laske nollia oikein
        Iterator<Boolean> colIterator = colIndList.iterator();
        ArrayList<Integer> freqList = new ArrayList<>();
        int col = 0;
        int count = 0;

        while (colIterator.hasNext()) {
            boolean include = colIterator.next();
            if (include) {

                int all = 0;
                count += calculateNullsCol(data.getDataList().get(col));

            }

            col++;
        }
        return count;

    }
 
    private int calculateNullsCol(Column column) {
        return column.getNullNo();              
    }
}

/* 
 * Copyright (C) 2018 Miika Tapio < miika.tapio at luke.fi > 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package remall;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * The allele recoding map.
 *
 * @author Miika Tapio < miika.tapio at luke.fi >
 */
public class AllMap {

    private  String filepathname;
    private final HashMap<MarkerAllele, String> allelemap;

    public AllMap(String filepathname) throws IOException {
        this.allelemap = new HashMap<>();

        try (
                BufferedReader bReader = new BufferedReader(new FileReader(filepathname))) {
            String line;
            line = bReader.readLine();
            String[] splitted = line.split("\\t", -1);

            if (!splitted[0].equals("Marker")) {
                throw new IllegalArgumentException(ErrorMessages.INMAP_ERROR); // The first header is not "Marker", suggests a wrong type of file
            }

            while ((line = bReader.readLine()) != null) {
                splitted = line.split("\\t", -1);
                if (splitted.length != 3) {
                    System.out.println("Virhe");
                    throw new IllegalArgumentException(ErrorMessages.INMAP_ERROR); // Does not have theree columns, more strict than initial check
                }

                if (splitted[0].isEmpty()) {
                    System.out.println("Virhe");
                    throw new IllegalArgumentException(ErrorMessages.INMAP_ERROR);                } else {

                    MarkerAllele hashkey = new MarkerAllele();
                    // 1
                    hashkey.setMarker(splitted[0]);
                    hashkey.setAllele(splitted[1]);
                    String checkifnull = allelemap.put(hashkey, splitted[2]);   //makes the hashmap
                    if (checkifnull != null) {
                        System.out.println("A double entry for microsatellite allele in map file!"); // double entry, possibly conflicting recoding instruction
                        throw new IllegalArgumentException(ErrorMessages.INMAP_ERROR);
                    }        
                }
            }   //while ends
        }
    }

    /**
     * Get the allele map
     *
     * @return
     */
    public HashMap<MarkerAllele, String> getAllelemap() {
        return allelemap;
    }

    /**
     * Get the new allele code as string based on the map file by giving marker
     * and old allele code as strings.
     *
     * @param marker
     * @param oldAllele
     * @return
     */
    public String getNewAllele(String marker, String oldAllele) {
        MarkerAllele searchkey = new MarkerAllele();
        searchkey.setMarker(marker); 
        searchkey.setAllele(oldAllele);
        String newAllele=allelemap.get(searchkey);
        return newAllele;
    }

    /**
     * Get the file name of the map
     *
     * @return
     */
    public String getFilepathname() {
        return filepathname;
    }

    @Override
    public String toString() {
        ArrayList<String> resultList = new ArrayList<>();
        allelemap.keySet().forEach((MarkerAllele name) -> {
            String key = name.toString();
            String value = allelemap.get(name);
            resultList.add(key + ";" + value + "\n");
        });
        String[] result = resultList.toArray(new String[resultList.size()]);
        Arrays.sort(result);
        StringBuilder resultSB = new StringBuilder();
        for (String result1 : result) {
            resultSB.append(result1);
        }
        return resultSB.toString();
    }

}

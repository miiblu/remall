/* 
 * Copyright (C) 2018 Miika Tapio < miika.tapio at luke.fi > 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package remall;  


import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

/**
 *
 * @author Miika Tapio < miika.tapio at luke.fi >
 */
public class Column {

    private byte[] header;
    private final ArrayList<byte[]> dataList = new ArrayList<>();

    public Column(String header) {
        this.header = header.getBytes(StandardCharsets.UTF_8);
    }

    /**
     * Can be used to change the header for a column. Several columns might have
     * same header. This method does not check for it!
     *
     * @param header
     */
    public void setHeader(String header) {
        this.header = header.getBytes(StandardCharsets.UTF_8);
    }

    /**
     * Add single string into the column
     *
     * @param data
     */
    public void addData(String data) {
        if (data != null) { 
            dataList.add(data.getBytes(StandardCharsets.UTF_8));
        } else {
            dataList.add(new byte[0]);
                    }
    }

    public void replaceLastData(String data) {
        dataList.set(dataList.size() - 1, data.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * Get header of the column
     *
     * @return Returns the header of the column (String).
     */
    public String getHeader() {
        return new String(header,StandardCharsets.UTF_8);
    }

    /**
     * Get the number of rows in column dataList
     *
     * @return int
     */
    public int getDataRowNo() {
        return dataList.size();
    }

    /**
     * Get data string at a specific index location of a column dataList.
     *
     * @param no
     * @return String
     */
    public String getData(int no) {
        return new String(dataList.get(no),StandardCharsets.UTF_8);
    }

    public ArrayList<byte[]> getDataList() {
        return dataList;
    }

    /* public String getDataColumnAsString() {
    return ";" + String.join(";;", dataList.stream().toString()) + ";";
    }*/
    
    public int getNullNo() { 
        int number=0;
        number = dataList.stream().filter((data) -> (new String(data,StandardCharsets.UTF_8).equals(""))).map((_item) -> 1).reduce(number, Integer::sum);
        return number;
    }

    @Override
    public String toString() {
        return getHeader() + ": " + dataList;
    }

}

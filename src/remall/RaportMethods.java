/* 
 * Copyright (C) 2018 Miika Tapio < miika.tapio at luke.fi > 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package remall;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author Miika Tapio < miika.tapio at luke.fi >
 */
public class RaportMethods {

    public static void printBasicFileMetadata(String pathname) throws IOException {
        BasicFileAttributes attr;
        Path path;
        path = Paths.get(pathname);
        attr = Files.readAttributes(path, BasicFileAttributes.class);
        System.out.println("File name: " + path);
        System.out.print("Created: " + attr.creationTime());
        System.out.print(" Modified: " + attr.lastModifiedTime());
        System.out.println(" Size: " + attr.size());
    }

    static void printChangeSummaryPerMarker(Marker oldmarker, ChangedMarker newmarker, AllMap map) {
        System.out.println("\n" + oldmarker.getMarkername()); // 
        System.out.print("Columns:");
        for (int k = 0; k < oldmarker.getColIndList().size(); k++) {
            int coli = k + 1;
            if (oldmarker.getColIndList().get(k).equals(true)) {
                System.out.print(" " + coli);
            }
        }
        System.out.print("\n");

        Iterator itrOld = oldmarker.getAlleleSet().iterator();

        ArrayList<String> newMarkerAlleleList = new ArrayList<>(newmarker.getAlleleSet());
        ArrayList<Integer> newMarkerAlleleFreqList = newmarker.getAlleleFreqList();
        HashMap<String, String> freqHash = new HashMap<>();
        Iterator itrnewMarkerAlleleList = newMarkerAlleleList.iterator();
        Iterator itrnewMarkerAlleleFreqList = newMarkerAlleleFreqList.iterator();
        while (itrnewMarkerAlleleList.hasNext()) {
            freqHash.put(itrnewMarkerAlleleList.next().toString(), itrnewMarkerAlleleFreqList.next().toString());
        }

        int current = 0;
        String newallele = null;
        //Boolean isMapped = true;
        while (itrOld.hasNext()) {
            String symbolIsMapped = "=";
            String symbolIsEqual = " =";

            String oldallele = itrOld.next().toString();
            newallele = map.getNewAllele(oldmarker.getMarkername(), oldallele);
            if (newallele == null) {
                newallele = oldallele;
                symbolIsMapped = "-";
            }
            String oldfreq = oldmarker.getAlleleFreqList().get(current++).toString();
            String newfreq = freqHash.get(newallele);
            if (newallele.equals(oldallele)) {
                symbolIsEqual = " <";
            }
            System.out.println(oldallele + "\t(" + oldfreq + ")\t" + symbolIsEqual + symbolIsMapped + ">\t" + newallele + "\t(" + newfreq + ")");
        }
    }

}

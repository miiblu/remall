/* 
 * Copyright (C) 2018 Miika Tapio < miika.tapio at luke.fi > 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package remall;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Miika Tapio < miika.tapio at luke.fi >
 */
public class Write {
/**
 * Writes the data matrix to a file.
 * @param newfilepathname
 * @param writtenList
 * @throws IOException 
 */
    static void datamatrix(String newfilepathname, DataClass writtenList) throws IOException {
        FileWriter fw;
        fw = new FileWriter(new File(newfilepathname));
        // Looping over all columns write headers            
        for (int col = 0; col < writtenList.size(); col++) {
            // Looping over all columns                    
            fw.write(writtenList.getHeaderList().get(col));
            if ((col + 1) < writtenList.size()) {
                fw.write("\t");
            }

        }
        // Looping over all columns write data            

        for (int row = 0; row < writtenList.getDataList().get(1).getDataRowNo(); row++) {
            for (int col = 0; col < writtenList.size(); col++) {
                if (col < writtenList.size() && col > 0) {
                    fw.write("\t");
                } else {
                    fw.write(System.lineSeparator());
                }
                fw.write(writtenList.getDataList().get(col).getData(row));
            }
        }
        fw.close();
        //System.out.println("Data matrix was written to file: " + newfilepathname);

    }
}
